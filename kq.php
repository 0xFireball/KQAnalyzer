<?php
//Set sessionID from urlparam
if (isset($_GET['sessid']))
    session_id($_GET['sessid']);

$trackingId = "UA-000-000-000"; //The tracking ID

session_start();

header("Access-Control-Allow-Origin: *");

include("config.php");
include("common.php");

$getUrl    = $_GET['url'];
$extradata = $_GET['eda']; //extra data

$trackr = $_SESSION["trackr"];
if (is_null($trackr)) {
    //assign tracker
    $_SESSION["trackr"] = genRandStr(12);
    $trackr             = $_SESSION["trackr"];
}

if (isset($_GET["tid"])) //The tracking ID
    $trackingId = $_GET["tid"];

/*
Collect data and tracking information about the user
*/
$remoteIP  = getRemoteIp();
$userAgent = $_SERVER['HTTP_USER_AGENT'];
$httpurl   = $_SERVER['HTTP_REFERER'];

$url = $getUrl;

if (is_null($url)) {
    if (is_null($httpurl)) {
        $url = "[unknown]";
    } else {
        $url = $httpurl;
    }
}

$timestamp = date("Y-m-d h:i:sa");

//$trackingData = "$remoteIP:$trackr visited $url --- Tracking Data: UserAgent: $userAgent\r\n";
$trackingData    = "$remoteIP:$trackr|$url|$timestamp|$trackingId|$extradata UserAgent: $userAgent\r\n";
$trackdbFileName = $remoteIP;

saveTrackingData($remoteIP, $trackingData);
sendFile("static/img/i.png", "image/png");

?>