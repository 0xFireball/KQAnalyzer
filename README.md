# KQAnalytics
KQ Data Analytics 2.0 - Open analytics for your web service!

## About
- KQ Data Analytics is a simple, open solution to track users on your web service. It is especially easy to track users, and it can be incorporated into web services and mobile apps by sending a GET request for an image. See [Quick Start](#quick-start) for install instructions.

# Quick Start
- You can install KQ Data Analytics on your server in 1 minute!
- Clone the repository (`git clone https://github.com/ZetaPhase/KQAnalytics.git`)
- Copy `config.example.php` to `config.php` and modify the configuration. Be sure to change the admin password.
- Add tracking images to your webpages (as simple as adding an IMG tag with `http://your-server.example.com/KQAnalytics/kq.php` as the source url. It will serve a 1x1 pixel image that will also be tracked, usually allowing the server to also capture data such as the client IP address and page that the user was visiting. An example tracker image would be: `<img src="http://your-server.example.com/KQAnalytics/kq.php" alt="">`
- You can also add tracking scripts to your sites. These allow you to collect much more information and detailed statistics like the links clicked on your site.
- To get the script to embed to enable script tracking, go to `http://your-server.example.com/KQAnalytics/admin/` and sign in with the password you set earlier. You should see the tracking script on the admin panel.
## Tips
- Cookie blocking extensions like `uMatrix` will often block cross-domain cookies, so install the analytics on the same domain as the application or web site to mitigate this issue.
- 
(c) ExaPhaser Industries, 0xFireball 2015-2016. All Rights Reserved.
