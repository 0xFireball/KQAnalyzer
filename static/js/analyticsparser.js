//AnalyticsLump is an interesting lump full of data to process and render!
var analyticsLump = analyticsPhpLump;
//console.log(analyticsLump);

function kq_deleteAllData()
{
    if (confirm("Are you sure? This is irreversible and will permanently remove all your data!"))
    {
        $.post("./action.php", {action: "rmalldata"}, function(data) {
            alert("Your data has been deleted. The page will be reloaded.");
            window.location.reload();
        });
    }
}