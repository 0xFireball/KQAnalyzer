//KQ Data Analytics Script
// Anonymous "self-invoking" function

/*global _kqdaq*//*global localStorage*//*global $*/

function mkuid(lnt) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < lnt; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

var kqserver = _kqdaq['server'];
var url = _kqdaq['url'];
var trackingId = _kqdaq['trackingId'];
var sessid = localStorage.getItem("sessid") || mkuid(26);
localStorage.setItem("sessid", sessid);

String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
        return typeof args[number] != 'undefined' ? args[number] : match;
    });
};

function startTracking() {
    $('a').on('click', function() {
        var clickedLink = $(this).attr('href');
        var linkTrackData = 'User clicked link to:  [{0}], leaving [{1}]'.format(clickedLink, url);
        var reqUrl = "{0}?eda={1}&url=LINKCLICK&sessid={2}&tid={3}".format(kqserver, encodeURIComponent(linkTrackData), sessid, trackingId);
        $.get(reqUrl);
    });
}

function sendAnalytics() {
    var reqUrl = "{0}?url={1}&sessid={2}&tid={3}".format(kqserver, encodeURIComponent(url), sessid, trackingId);
    $.get(reqUrl);
    startTracking();
}

(function() {
    // Load the script
    var script = document.createElement("SCRIPT");
    script.src = 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existance
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() {
                checkReady(callback);
            }, 100);
        }
    };

    // Start polling...
    checkReady(function($) {
        sendAnalytics();
    });
})();