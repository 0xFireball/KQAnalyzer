<?php
session_start();
$noEcho = true;
include(dirname(__DIR__)."/config.php");
$providedApiKey = $_GET['apikey'];
//You can authenticate yourself with the API key or your session KEY
$accessApproved = in_array($providedApiKey, $apiKeys) || $_SESSION["loggedin"];
if (!$accessApproved)
{
    header('HTTP/1.0 403 Forbidden');
    echo 'Authentication failure.';
    die();
}
else 
{
    //Access is approved
    header('Content-type: application/json');
    include(__DIR__."/scripts/kqdata.php");
}
?>