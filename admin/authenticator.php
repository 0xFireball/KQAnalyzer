<?php
include("auth.php");
session_start();
if (!$_SESSION["loggedin"])
{
    //not logged in.
    header("Location: login.php");
    die();
}
?>