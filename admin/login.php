<?php
session_start();
include("auth.php");
$cpw = $_POST['cpw'];
if (!is_null($cpw))
{
  loginUser($cpw);
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login - KQ Analytics Admin</title>
    <link rel="stylesheet" href="../static/css/foundation.css">
    <link rel="stylesheet" href="../static/css/app.css">
</head>

<body>

    <div class="row">
        <div class="large-12 columns">
            <h1>KQ Analytics Admin</h1>
        </div>
    </div>

    <div class="row">
        <div class="large-12 columns">
            <div class="callout">
                <h5>Please log in:</h5>
                <form action="login.php" method="post" id="loginform">
                    <div class="row">
                        <div class="large-12 columns">
                            <div class="row collapse">
                                <div class="small-10 columns">
                                    <input type="password" name="cpw" placeholder="Admin Password">
                                </div>
                                <div class="small-2 columns">
                                    <a href="#" id="loginbtn" class="button postfix">Go</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="../static/js/vendor/jquery.js"></script>
    <script src="../static/js/vendor/what-input.js"></script>
    <script src="../static/js/vendor/foundation.js"></script>
    <script src="../static/js/app.js"></script>
    <script>
      $("#loginbtn").click(function() {
        $("#loginform").submit();
      });
    </script>
</body>

</html>