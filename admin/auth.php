<?php
include(dirname(__DIR__)."/config.php");
session_start();

function loginUser($cpw)
{
    global $admin_password;
    if ($admin_password == $cpw)
    {
        //Login success
        $_SESSION["loggedin"] = true;
        header("Location: index.php");
    }
    else 
    {
        //Login failed.
    }
}

function logoutUser()
{
    $_SESSION["loggedin"] = null;
}
?>