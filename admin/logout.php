<?php
include("auth.php");
session_start();
if ($_SESSION["loggedin"])
{
    $_SESSION["loggedin"] = null; //Log Out user.
    header("Location: login.php");
    die();
}
?>