<?php
include(dirname(dirname(__DIR__))."/config.php");
include(dirname(dirname(__DIR__))."/common.php");
if (!$noEcho) {
    //echo
    include("scriptauthenticator.php");
}
$dataTables    = "";
$injScript     = "";
$analyticsBlob = array();
$trkFiles      = array();
foreach (glob("$data_collection_root/*.trk") as $file) {
    $trkFiles[] = $file;
}
$analyticsBlob["rawData"] = array();
foreach ($trkFiles as $trackingDataFile) {
    $file = fopen($trackingDataFile, "r");
    while (!feof($file)) {
        $line = fgets($file);
        if (!ctype_space($line)) {
            $rawTrackingData  = htmlspecialchars($line);
            $trackingDataBlob = array_filter(explode("|", $rawTrackingData));
            $IpAndId          = array_filter(explode(":", $trackingDataBlob[0]));
            $ip               = $IpAndId[0];
            if ($ip != "") {
                $id             = $IpAndId[1];
                $url            = $trackingDataBlob[1];
                $timestamp      = $trackingDataBlob[2];
                $trackingId     = $trackingDataBlob[3];
                $extradata      = $trackingDataBlob[4];
                $uid            = genRandStr(24);
                $b64extradata   = base64_encode($extradata);
                $tkDataChartRow = "<tr><td>$ip</td><td>$id<br>$trackingId</td><td>$url (<a id=\"$uid\" class=\"moreBtn\">more</a>)</td><td>$timestamp</td></tr>\n";
                $injScript .= "$('#$uid').attr('kq-extra-data','$b64extradata').click(function() {
$('#moreInfoModalText').html(atob($(this).attr('kq-extra-data')));
$('#moreInfoModal').animateCss('fadeIn');
});\n";

                $dataTables .= $tkDataChartRow;
                $trackingDataBlobOrganized = array(
                  "id" => $id,
                  "url" => $url,
                  "timestamp" => $timestamp,
                  "b64extradata" => $b64extradata,
                  "trackingid" => $trackingId,
                );
                $analyticsBlob["rawData"][] = $trackingDataBlobOrganized;
            }
        }
    }
    fclose($file);
}

$analyticsBlobJson = json_encode($analyticsBlob);

$script = <<<JS

var rawDataBox = $("#raw-a-data");
rawDataBox.html("Creating data tables...");
var kqDataTable = `
<table>
  <thead>
    <tr>
      <th width="150">IP Address</th>
      <th width="150">Identifier</th>
      <th width="250">Payload</th>
      <th width="100">Timestamp</th>
    </tr>
  </thead>
  <tbody>
  $dataTables
  </tbody>
</table>
`;
rawDataBox.html(kqDataTable);
$injScript
var analyticsPhpLump = $analyticsBlobJson;
JS;

if (!$noEcho) {
    //echo
    header("Content-type: text/javascript");
    echo $script;
}
else {
    //Don't Echo
    echo $analyticsBlobJson;
}

die();

?>