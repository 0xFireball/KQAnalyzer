<?php include("authenticator.php"); include(dirname(__DIR__)."/common.php") ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>KQ Analytics Admin</title>
    <link rel="stylesheet" href="../static/css/foundation.css">
    <link rel="stylesheet" href="../static/css/app.css">
    <link rel="stylesheet" href="../static/css/animate.min.css">
</head>

<body>

    <div class="row">
        <div class="large-12 columns">
            <div class="callout">
                <h3>KQ Data Analytics Admin Interface</h3>
                <p>KQ Analytics is still in an early stage of development, and this admin interface will change significantly as the software improves. Please refer to the following resources:</p>
                <div class="row">
                    <div class="large-4 medium-4 columns">
                        <p><a href="https://github.com/ZetaPhase/KQAnalytics/wiki/Admin-Guide">KQ Analytics Admin Guide</a><br />Everything you need to know about being an admin on KQ Analytics.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row" id="settingspanel" style="display: none">
        <div class="large-12 columns">
            <div class="callout primary">
                <h3>Settings</h3>
                <p>
                    Caution: improper use of this tool can and likely will result in <b>severe data loss</b>! Please ensure you know what you are doing.
                </p>
                <div class="row">
                    <div class="large-4 medium-4 columns">
                        <p><a class="alert button" href="javascript:kq_deleteAllData()">Delete All Data</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="large-12 medium-12 columns">
            <div class="row">
                <div class="large-8 columns">
                    <h5>Tracking snippet</h5>
                    <p>
                        Embed this snippet into your HTML pages to automatically send advanced analytics data to KQ.
                    </p>
                    <div class="callout">
                        <pre>
<?php
$kqroot = dirname(dirname(getCurrentUrl()));
$script = <<<JS
<script>
    var _kqdaq = _kqdaq || {};
    var kqscript = '$kqroot/static/js/kqda.js';
    _kqdaq['server'] = '$kqroot/kq.php';
    _kqdaq['url'] = document.location.href;
    _kqdaq['trackingId'] = "UA-111-111-111";

    (function() {
        var kqda = document.createElement('script');
        kqda.type = 'text/javascript';
        kqda.async = true;
        kqda.src = kqscript;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(kqda, s);
    })();
</script>
JS;
echo(htmlspecialchars($script));

?>
                        </pre>
                    </div>
                </div>
                <div class="large-4 medium-4 columns">
                    <h5>Actions</h5>
        
                    <a href="logout.php" class="button alert">Log Out</a>
                    <a href="javascript:showSettings()" class="button">Settings</a>
                    <div class="callout">
                        <h5>Help improve KQ Analytics!</h5>
                        <p>
                            KQ Analytics is developed by <a target="blank" href="https://zetaphase.io">ZetaPhase Technologies</a>, a small, independent developer group. It is completely open-source, and everyone is welcome to contribute.
                        </p>
                        <a target="blank" href="https://github.com/ZetaPhase/KQAnalytics" class="small button">Go to KQ Repository</a>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="large-12 columns">
                    <h5>Raw Analytics Data - All Time</h5><a class="button small" href="javascript:$('#raw-data-and-more').toggle()">Toggle Visibility</a>
                    <div class="row" id="raw-data-and-more">
                        <div class="large-8 columns">
                            <div class="primary callout" id="raw-data-container">
                                <div id="raw-a-data">
                                    Loading...
                                </div>
                            </div>
                        </div>
                        <div class="large-4 columns">
                            <div class="callout" id="moreInfoModal">
                            <!--More Info Modal-->
                            <div class="reveal-modal" aria-hidden="true" role="dialog">
                                  <h2 id="moreInfoModalTitle">More Data</h2>
                                  <p id="moreInfoModalText">
                                      Click the <strong>more</strong> link in the table on the left to display additional data.
                                  </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <hr />

            <h5>We bet you&rsquo;ll need a form somewhere:</h5>
            <form>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Input Label</label>
                        <input type="text" placeholder="large-12.columns" />
                    </div>
                </div>
                <div class="row">
                    <div class="large-4 medium-4 columns">
                        <label>Input Label</label>
                        <input type="text" placeholder="large-4.columns" />
                    </div>
                    <div class="large-4 medium-4 columns">
                        <label>Input Label</label>
                        <input type="text" placeholder="large-4.columns" />
                    </div>
                    <div class="large-4 medium-4 columns">
                        <div class="row collapse">
                            <label>Input Label</label>
                            <div class="input-group">
                                <input type="text" placeholder="small-9.columns" class="input-group-field" />
                                <span class="input-group-label">.com</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Select Box</label>
                        <select>
                <option value="husker">Husker</option>
                <option value="starbuck">Starbuck</option>
                <option value="hotdog">Hot Dog</option>
                <option value="apollo">Apollo</option>
              </select>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 medium-6 columns">
                        <label>Choose Your Favorite</label>
                        <input type="radio" name="pokemon" value="Red" id="pokemonRed"><label for="pokemonRed">Radio 1</label>
                        <input type="radio" name="pokemon" value="Blue" id="pokemonBlue"><label for="pokemonBlue">Radio 2</label>
                    </div>
                    <div class="large-6 medium-6 columns">
                        <label>Check these out</label>
                        <input id="checkbox1" type="checkbox"><label for="checkbox1">Checkbox 1</label>
                        <input id="checkbox2" type="checkbox"><label for="checkbox2">Checkbox 2</label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Textarea Label</label>
                        <textarea placeholder="small-12.columns"></textarea>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src="../static/js/vendor/jquery.js"></script>
    <script src="../static/js/vendor/what-input.js"></script>
    <script src="../static/js/vendor/foundation.js"></script>
    <script src="../static/js/app.js"></script>
    <script src="../static/js/admin.js"></script>
    <script>
        $("#loginbtn").click(function() {
            $("#loginform").submit();
        });
        $.fn.extend({
            animateCss: function (animationName) {
                var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                $(this).addClass('animated ' + animationName).one(animationEnd, function() {
                    $(this).removeClass('animated ' + animationName);
                });
            }
        });
        
        function showSettings() {
            $("#settingspanel").slideToggle();
        }
    </script>
    
    <script src="scripts/kqdata.php"></script>
    <script src="../static/js/analyticsparser.js"></script>
</body>

</html>