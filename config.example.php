<?php

//The root folder to store data. Do not include a trailing slash.
$data_collection_root = __DIR__."/kqdb";

//The admin password. Make sure you change this!
//This will grant access to some basic data on the admin page.
$admin_password = "password";

//The URL to redirect users to when they visit index pages
$indexRedirectUrl = "https://github.com/ZetaPhase/KQAnalytics";

// API keys to access the API. By default, this is an empty array.
// You must add some keys to enable access through an application like KQ Analyzer
// Example:
// $apiKeys = array('1234','5678');
// We strongly recommend using API keys with a length of about 40 characters for maximum security
$apiKeys = array();
?>